package pl.testy.qfII.model;

import pl.testy.qfII.quant_pack.basic_objects.OptionBehaviour;
import pl.testy.qfII.quant_pack.basic_objects.SingleOption;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

// This object collects and saves all data entered by the user
public class InputData {
    private static final int maxNumberOfSimulations = 1_000_000;
    private static final int maxNumberOfIntervals = 1_000;

    // input data from step 1
    private OptionBehaviour optionBehaviour;
    private BigDecimal spotPrice;
    private BigDecimal riskFreeRateAsPercent;
    private BigDecimal annualizedVolatilityAsPercent;
    private BigDecimal annualizedDividendAsPercent;
    private BigDecimal timeToMaturityInYears;

    // input data from step 2
    private List<SingleOption> optionsInStrategy;

    // input data from step 3
    private int numberOfIntervals;
    private int numberOfSimulations;

    //
    // SIMPLE GETTERS
    //
    public OptionBehaviour getOptionBehaviour() {
        return optionBehaviour;
    }

    public BigDecimal getSpotPrice() {
        return spotPrice;
    }

    public BigDecimal getRiskFreeRateAsPercent() {
        return riskFreeRateAsPercent;
    }

    public BigDecimal getAnnualizedVolatilityAsPercent() {
        return annualizedVolatilityAsPercent;
    }

    public BigDecimal getAnnualizedDividendAsPercent() {
        return annualizedDividendAsPercent;
    }

    public BigDecimal getTimeToMaturityInYears() {
        return timeToMaturityInYears;
    }

    public List<SingleOption> getOptionsInStrategy() {
        return optionsInStrategy;
    }

    public int getNumberOfIntervals() {
        return numberOfIntervals;
    }

    public int getNumberOfSimulations() {
        return numberOfSimulations;
    }

    //
    // SIMPLE SETTERS
    //
    public void setOptionBehaviour(OptionBehaviour optionBehaviour) {
        this.optionBehaviour = optionBehaviour;
    }

    public void setSpotPrice(BigDecimal spotPrice) {
        this.spotPrice = spotPrice;
    }

    public void setRiskFreeRateAsPercent(BigDecimal riskFreeRateAsPercent) {
        this.riskFreeRateAsPercent = riskFreeRateAsPercent;
    }

    public void setAnnualizedVolatilityAsPercent(BigDecimal annualizedVolatilityAsPercent) {
        this.annualizedVolatilityAsPercent = annualizedVolatilityAsPercent;
    }

    public void setAnnualizedDividendAsPercent(BigDecimal annualizedDividendAsPercent) {
        this.annualizedDividendAsPercent = annualizedDividendAsPercent;
    }

    public void setTimeToMaturityInYears(BigDecimal timeToMaturityInYears) {
        this.timeToMaturityInYears = timeToMaturityInYears;
    }

    public void setOptionsInStrategy(int numberOfOptionsInStrategy) {
        optionsInStrategy = new ArrayList<>(numberOfOptionsInStrategy);
        for (int i = 0; i < numberOfOptionsInStrategy; i++) {
            optionsInStrategy.add(new SingleOption());
        }
    }

    public void setNumberOfOptionsInStratgy(int numberOfOptionsInStrategy) {
        setOptionsInStrategy(numberOfOptionsInStrategy);
    }

    public void setNumberOfIntervals(int numberOfIntervals) {
        if (numberOfIntervals > maxNumberOfIntervals) {
            this.numberOfIntervals = maxNumberOfIntervals;
        } else {
            this.numberOfIntervals = numberOfIntervals;
        }
    }

    public void setNumberOfSimulations(int numberOfSimulations) {
        if (numberOfSimulations > maxNumberOfSimulations) {
            this.numberOfSimulations = maxNumberOfSimulations;
        } else {
            this.numberOfSimulations = numberOfSimulations;
        }
    }

    //
    // MODIFIED GETTERS
    //
    public double getSpotAsDouble() {
        return spotPrice.doubleValue();
    }

    public double getRiskFreeAsNumberAndDouble() {
        return riskFreeRateAsPercent.doubleValue() / 100.0;
    }

    public double getAnnualizedVolatilityAsNumberAndDouble() {
        return annualizedVolatilityAsPercent.doubleValue() / 100.0;
    }

    public double getAnnualizedDividendAsNumberAndDouble() {
        return annualizedDividendAsPercent.doubleValue() / 100.0;
    }

    public double getTimeToMaturityAsDouble() {
        return timeToMaturityInYears.doubleValue();
    }

    public int getNumberOfOptionsInStratgy() {
        if (optionsInStrategy == null) {
            return 1;
        } else {
            return optionsInStrategy.size();
        }
    }
}
