package pl.testy.qfII.model;

import java.nio.file.Path;

public class OutputData {
    private Path finalReportPath;

    public Path getFinalReportPath() {
        return finalReportPath;
    }

    public void setFinalReportPath(Path finalReportPath) {
        this.finalReportPath = finalReportPath;
    }
}
