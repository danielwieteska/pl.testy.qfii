package pl.testy.qfII.quant_pack.settlement_policy;

class EuropeanPolicy extends SettlementPolicy {
    EuropeanPolicy(double[] initialPrices) {
        super(initialPrices);
    }

    @Override
    public void addNewPrices(double[] newPrices) {
        settlementPrices = newPrices;
    }
}
