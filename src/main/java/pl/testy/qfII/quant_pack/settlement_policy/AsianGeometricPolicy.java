package pl.testy.qfII.quant_pack.settlement_policy;

import static java.lang.Math.pow;

class AsianGeometricPolicy extends SettlementPolicy {
    private int stepCounter;

    AsianGeometricPolicy(double[] initialPrices) {
        super(initialPrices);
        stepCounter = 1;
    }

    @Override
    public void addNewPrices(double[] newPrices) {
        for (int i = 0; i < settlementPrices.length; i++) {
            settlementPrices[i] = pow(
                    pow(settlementPrices[i], stepCounter - 1.0) * newPrices[i],
                    1.0 / stepCounter
            );
        }
        stepCounter++;
    }
}
