package pl.testy.qfII.quant_pack.settlement_policy;

import pl.testy.qfII.quant_pack.basic_objects.OptionBehaviour;

public class SettlementPolicyFactory {
    public static SettlementPolicy getSettlementPolicy(OptionBehaviour optionBehaviour, double[] initialPrices) {
        switch (optionBehaviour) {
            case EUROPEAN:
                return new EuropeanPolicy(initialPrices);
            case ASIAN_ARITHMETIC:
                return new AsianArithmeticPolicy(initialPrices);
            case ASIAN_GEOMETRIC:
            default:
                return new AsianGeometricPolicy(initialPrices);
        }
    }
}
