package pl.testy.qfII.quant_pack.settlement_policy;

class AsianArithmeticPolicy extends SettlementPolicy {
    private int stepCounter;

    AsianArithmeticPolicy(double[] initialPrices) {
        super(initialPrices);
        stepCounter = 1;
    }

    @Override
    public void addNewPrices(double[] newPrices) {
        for (int i = 0; i < settlementPrices.length; i++) {
            settlementPrices[i] = (settlementPrices[i] * ((stepCounter - 1.0) /  stepCounter)) +
                    (newPrices[i] * (1.0 / stepCounter));
        }
        stepCounter++;
    }
}
