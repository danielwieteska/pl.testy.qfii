package pl.testy.qfII.quant_pack.settlement_policy;

import java.util.Arrays;

public abstract class SettlementPolicy {
    double[] settlementPrices;

    SettlementPolicy(double[] initialPrices) {
        settlementPrices = Arrays.copyOf(initialPrices, initialPrices.length);
    }

    public abstract void addNewPrices(double[] newPrices);

    public double[] getSettlementPrices() {
        return settlementPrices;
    }
}
