package pl.testy.qfII.quant_pack.basic_objects;

public enum TradePosition {
    LONG("long"),
    SHORT("short");

    private String legibleName;

    TradePosition(String legibleName) {
        this.legibleName = legibleName;
    }

    public String getLegibleName() {
        return legibleName;
    }
}
