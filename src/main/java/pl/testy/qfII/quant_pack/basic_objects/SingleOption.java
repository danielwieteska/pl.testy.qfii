package pl.testy.qfII.quant_pack.basic_objects;

import pl.testy.qfII.quant_pack.helpers.Helpers;

import java.math.BigDecimal;
import java.util.Arrays;

public class SingleOption {
    private BigDecimal strike;
    private OptionType optionType;
    private TradePosition tradePosition;
    private double currentPrice;
    private double stdDev;

    public BigDecimal getStrike() {
        return strike;
    }

    public void setStrike(BigDecimal strike) {
        this.strike = strike;
    }

    public OptionType getOptionType() {
        return optionType;
    }

    public void setOptionType(OptionType optionType) {
        this.optionType = optionType;
    }

    public TradePosition getTradePosition() {
        return tradePosition;
    }

    public void setTradePosition(TradePosition tradePosition) {
        this.tradePosition = tradePosition;
    }

    public double getCurrentPrice() {
        return currentPrice;
    }

    public double getStdDev() {
        return stdDev;
    }

    public void calculatePriceAndStdDev(double[] settlementPrices, double discountFactor) {
        double[] simulatedOptionPrices = Arrays.stream(settlementPrices)
                .map(x -> calculatePayoff(x) * discountFactor)
                .toArray();
        currentPrice = Helpers.calculateMean(simulatedOptionPrices);
        stdDev = Helpers.calculateStdDev(simulatedOptionPrices, currentPrice);
    }

    public double calculatePayoff(double settlementPrice) {
        if (optionType == OptionType.CALL && tradePosition == TradePosition.LONG) {
            return (settlementPrice - strike.doubleValue()) > 0 ? (settlementPrice - strike.doubleValue()) : 0;
        } else if (optionType == OptionType.CALL && tradePosition == TradePosition.SHORT) {
            return (settlementPrice - strike.doubleValue()) > 0 ? (-settlementPrice + strike.doubleValue()) : 0;
        } else if (optionType == OptionType.PUT && tradePosition == TradePosition.LONG) {
            return (-settlementPrice + strike.doubleValue()) > 0 ? (-settlementPrice + strike.doubleValue()) : 0;
        } else if (optionType == OptionType.PUT && tradePosition == TradePosition.SHORT) {
            return (-settlementPrice + strike.doubleValue()) > 0 ? (settlementPrice - strike.doubleValue()) : 0;
        } else {
            throw new AssertionError("Something goes wrong!");
        }
    }
}
