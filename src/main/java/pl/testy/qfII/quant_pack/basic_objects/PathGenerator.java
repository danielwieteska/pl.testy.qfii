package pl.testy.qfII.quant_pack.basic_objects;

import pl.testy.qfII.model.InputData;

import java.util.Random;

import static java.lang.Math.exp;
import static java.lang.Math.sqrt;

public class PathGenerator {
    private static final Random random = new Random();

    private int numberOfSimulations;
    private double constantPartOfPath;
    private double randomPartOfPath;

    public PathGenerator(InputData inputData) {
        this.numberOfSimulations = inputData.getNumberOfSimulations();
        double timeStep = inputData.getTimeToMaturityAsDouble() / inputData.getNumberOfIntervals();
        this.constantPartOfPath = (inputData.getRiskFreeAsNumberAndDouble()
                - inputData.getAnnualizedDividendAsNumberAndDouble()
                - 0.5 * inputData.getAnnualizedVolatilityAsNumberAndDouble() * inputData.getAnnualizedVolatilityAsNumberAndDouble()
        ) * timeStep;
        this.randomPartOfPath = inputData.getAnnualizedVolatilityAsNumberAndDouble() * sqrt(timeStep);
    }

    // PRIVATE METHODS
    public double[] moveForwardPricesByOneTimeStep(double[] currentPrices) {
        for (int i = 0; i < numberOfSimulations; i++) {
            currentPrices[i] = currentPrices[i] * getRandomMultiplier();
        }
        return currentPrices;
    }

    public double[] getInitialPrices(double initialPrice) {
        double[] prices = new double[numberOfSimulations];
        for (int i = 0; i < numberOfSimulations; i++) {
            prices[i] = initialPrice;
        }
        return prices;
    }

    // PRIVATE METHODS
    private double getRandomMultiplier() {
        return exp(constantPartOfPath + (randomPartOfPath * random.nextGaussian()));
    }
}
