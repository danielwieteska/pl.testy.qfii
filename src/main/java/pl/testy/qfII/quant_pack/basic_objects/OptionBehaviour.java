package pl.testy.qfII.quant_pack.basic_objects;

public enum OptionBehaviour {
    EUROPEAN("european"),
    ASIAN_ARITHMETIC("asian-arithmetic"),
    ASIAN_GEOMETRIC("asian-geometric");

    private String legibleName;

    OptionBehaviour(String legibleName) {
        this.legibleName = legibleName;
    }

    public String getLegibleName() {
        return legibleName;
    }
}
