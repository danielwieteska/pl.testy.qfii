package pl.testy.qfII.quant_pack.basic_objects;

public enum OptionType {
    CALL("call"),
    PUT("put");

    private String legibleName;

    OptionType(String legibleName) {
        this.legibleName = legibleName;
    }

    public String getLegibleName() {
        return legibleName;
    }
}
