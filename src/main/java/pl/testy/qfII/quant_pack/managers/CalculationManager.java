package pl.testy.qfII.quant_pack.managers;

import pl.testy.qfII.model.InputData;
import pl.testy.qfII.quant_pack.basic_objects.PathGenerator;
import pl.testy.qfII.quant_pack.basic_objects.SingleOption;
import pl.testy.qfII.quant_pack.helpers.Constants;
import pl.testy.qfII.quant_pack.helpers.Helpers;
import pl.testy.qfII.quant_pack.settlement_policy.SettlementPolicy;
import pl.testy.qfII.quant_pack.settlement_policy.SettlementPolicyFactory;

import java.util.Arrays;

import static java.lang.Math.exp;

public class CalculationManager {
    private InputData inputData;
    private PathGenerator pathGenerator;
    private double[][] strategyPriceSurface;
    private double discountFactorHelper;
    private double strategyPrice;
    private double strategyStdDev;
    private double computationTimeInSeconds;

    public CalculationManager(InputData inputData) {
        this.inputData = inputData;
        pathGenerator = new PathGenerator(inputData);
        strategyPriceSurface = new double[inputData.getNumberOfIntervals() + 1][Constants.numberOfSpotPricesOnSurface];
        discountFactorHelper = inputData.getRiskFreeAsNumberAndDouble()
                * (inputData.getTimeToMaturityAsDouble() / inputData.getNumberOfIntervals());
    }

    public double[][] getStrategyPriceSurface() {
        long startTime = System.nanoTime();
        double[] finalSettlementPrices = new double[0];
        for (int spotStep = 0; spotStep < Constants.numberOfSpotPricesOnSurface; spotStep++) {
            double partOfInitialSpot = getPartOfInitialSpot(spotStep);
            double[] setOfPrices = getInitialPrices(partOfInitialSpot);
            SettlementPolicy settlementPolicy = getSettlementPolicy(setOfPrices);
            for (int ttmStep = 0; ttmStep <= inputData.getNumberOfIntervals(); ttmStep++) {
                double discountFactor = exp(-ttmStep * discountFactorHelper);
                double[] settlementPrices = settlementPolicy.getSettlementPrices();
                strategyPriceSurface[ttmStep][spotStep] = getStrategyPrice(settlementPrices, discountFactor);
                if (partOfInitialSpot == 1.0 && ttmStep == inputData.getNumberOfIntervals()) {
                    finalSettlementPrices = settlementPrices;
                }
                setOfPrices = pathGenerator.moveForwardPricesByOneTimeStep(setOfPrices);
                settlementPolicy.addNewPrices(setOfPrices);
            }
        }
        calculateFinalPricesAndStatMeasures(
                finalSettlementPrices,
                exp(-inputData.getNumberOfIntervals() * discountFactorHelper)
        );
        long endTime = System.nanoTime();
        computationTimeInSeconds = (endTime - startTime) / 1_000_000_000D;
        return strategyPriceSurface;
    }

    public double getStrategyPrice() {
        return strategyPrice;
    }

    public double getStrategyStdDev() {
        return strategyStdDev;
    }

    public double getComputationTimeInSeconds() {
        return computationTimeInSeconds;
    }

    // PRIVATE METHODS
    private double getPartOfInitialSpot(int spotStep) {
        return Constants.initialSpotPriceOnSurfaceAsPercentOfCurrentSpot
                + (spotStep * Constants.percentStepOfSpotPricesOnSurface);
    }

    private double[] getInitialPrices(double partOfInitialSpot) {
        return pathGenerator.getInitialPrices(partOfInitialSpot * inputData.getSpotAsDouble());
    }

    private SettlementPolicy getSettlementPolicy(double[] initialPrices) {
        return SettlementPolicyFactory.getSettlementPolicy(inputData.getOptionBehaviour(), initialPrices);
    }

    private void calculateFinalPricesAndStatMeasures(double[] settlementPrices, double discountFactor) {
        // price and std dev for each option
        inputData.getOptionsInStrategy()
                .forEach(x -> x.calculatePriceAndStdDev(settlementPrices, discountFactor));
        // price and std dev for all strategy
        double[] strategyPrices = new double[inputData.getNumberOfSimulations()];
        for (int i = 0; i < inputData.getNumberOfSimulations(); i++) {
            int k = i;
            strategyPrices[i] = inputData.getOptionsInStrategy().stream()
                    .mapToDouble(x -> x.calculatePayoff(settlementPrices[k]) * discountFactor)
                    .sum();
        }
        strategyPrice = Helpers.calculateMean(strategyPrices);
        strategyStdDev = Helpers.calculateStdDev(strategyPrices, strategyPrice);
    }

    private double getStrategyPrice(double[] settlementPrices, double discountFactor) {
        double rawStrategyValue = inputData.getOptionsInStrategy().stream()
                .mapToDouble(x -> getSimulatedOptionPriceWithoutDiscountFactor(settlementPrices, x))
                .sum();
        return discountFactor * rawStrategyValue;
    }

    private double getSimulatedOptionPriceWithoutDiscountFactor(double[] settlementPrices, SingleOption singleOption) {
        double sumOfPayments = Arrays.stream(settlementPrices)
                .map(singleOption::calculatePayoff)
                .sum();
        return sumOfPayments / inputData.getNumberOfSimulations();
    }
}
