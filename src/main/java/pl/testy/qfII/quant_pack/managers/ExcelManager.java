package pl.testy.qfII.quant_pack.managers;

import pl.progsol.core.exceling.CellAddress;
import pl.progsol.core.exceling.Excel;
import pl.progsol.core.exceling.ExcelWriter;
import pl.testy.qfII.model.InputData;
import pl.testy.qfII.quant_pack.basic_objects.SingleOption;
import pl.testy.qfII.quant_pack.helpers.Constants;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class ExcelManager {
    private Path finalReport;

    public ExcelManager(String remoteAddr) {
        Path helperPath = Paths.get(Constants.rootForFile, remoteAddr);
        int noo = 0;
        if (helperPath.toFile().exists()) {
            File[] dirs = helperPath.toFile().listFiles(File::isDirectory);
            for (File dir : dirs) {
                int no = Integer.parseInt(dir.getName());
                if (no > noo) {
                    noo = no;
                }
            }
        }
        noo++;
        finalReport = Paths.get(helperPath.toString(), "" + noo);
        finalReport.toFile().mkdirs();
        finalReport = Paths.get(finalReport.toString(), Constants.outputFileName);
        exportResource(Constants.xlsmScheme);
    }

    public void createReportInExcel(InputData inputData) {

        CalculationManager calculationsManager = new CalculationManager(inputData);
        double[][] results = calculationsManager.getStrategyPriceSurface();

        List<Object[]> formattedResults = new ArrayList<>(results.length);
        for (int rowIndex = 0; rowIndex < results.length; rowIndex++) {
            double[] row = results[rowIndex];
            Object[] formattedRow = new Object[row.length];
            for (int columnIndex = 0; columnIndex < row.length; columnIndex++) {
                formattedRow[columnIndex] = row[columnIndex];
            }
            formattedResults.add(formattedRow);
        }

        ExcelWriter excelWriter = Excel.newWriter(finalReport);
        excelWriter.open();
        //
        inputData.getOptionsInStrategy().sort(Comparator.comparingDouble(o -> o.getStrike().doubleValue()));
        // // //
        excelWriter.selectSheet("FINAL-REPORT");
        excelWriter.setCellValue(CellAddress.colA_row1("G", 8), inputData.getOptionBehaviour().getLegibleName());
        excelWriter.setCellValue(CellAddress.colA_row1("G", 9), inputData.getSpotPrice().doubleValue());
        excelWriter.setCellValue(CellAddress.colA_row1("G", 10), inputData.getRiskFreeRateAsPercent().doubleValue());
        excelWriter.setCellValue(CellAddress.colA_row1("G", 11), inputData.getAnnualizedVolatilityAsPercent().doubleValue());
        excelWriter.setCellValue(CellAddress.colA_row1("G", 12), inputData.getAnnualizedDividendAsPercent().doubleValue());
        excelWriter.setCellValue(CellAddress.colA_row1("G", 13), inputData.getTimeToMaturityInYears().doubleValue());
        excelWriter.setCellValue(CellAddress.colA_row1("G", 14), inputData.getNumberOfIntervals());
        excelWriter.setCellValue(CellAddress.colA_row1("G", 15), inputData.getNumberOfSimulations());
        excelWriter.setCellValue(CellAddress.colA_row1("G", 16), calculationsManager.getComputationTimeInSeconds());

        int i = 0;
        for (SingleOption singleOption : inputData.getOptionsInStrategy()) {
            excelWriter.setCellValue(CellAddress.colA_row1("D", 21 + i), singleOption.getStrike().doubleValue());
            excelWriter.setCellValue(CellAddress.colA_row1("E", 21 + i), singleOption.getOptionType().getLegibleName());
            excelWriter.setCellValue(CellAddress.colA_row1("F", 21 + i), singleOption.getTradePosition().getLegibleName());
            excelWriter.setCellValue(CellAddress.colA_row1("G", 21 + i), singleOption.getCurrentPrice());
            excelWriter.setCellValue(CellAddress.colA_row1("H", 21 + i), singleOption.getStdDev());
            i++;
        }
        excelWriter.setCellValue(CellAddress.colA_row1("G", 31), calculationsManager.getStrategyPrice());
        excelWriter.setCellValue(CellAddress.colA_row1("H", 31), calculationsManager.getStrategyStdDev());

        // // //
        List<SingleOption> options = inputData.getOptionsInStrategy();
        excelWriter.selectSheet("TECHNICAL");
        int k = 0;
        excelWriter.setCellValue(CellAddress.colA_row1("D", 9), 0.0);
        excelWriter.setCellValue(CellAddress.colA_row1("E", 9), getAggregatedPayoff(options.toArray(new SingleOption[options.size()]), 0.0));
        k++;
        for (SingleOption singleOption : inputData.getOptionsInStrategy()) {
            excelWriter.setCellValue(CellAddress.colA_row1("D", 9 + k), singleOption.getStrike().doubleValue());
            excelWriter.setCellValue(CellAddress.colA_row1("E", 9 + k), getAggregatedPayoff(options.toArray(new SingleOption[options.size()]), singleOption.getStrike().doubleValue()));
            //
            excelWriter.setCellValue(CellAddress.colA_row1("O", 8 + k), getAggregatedPayoff(new SingleOption[]{singleOption}, 0.0));
            excelWriter.setCellValue(CellAddress.colA_row1("Q", 8 + k), getAggregatedPayoff(new SingleOption[]{singleOption}, 1_000_000.0));
            k++;
        }
        excelWriter.setCellValue(CellAddress.colA_row1("D", 9 + k), 1_000_000.0);
        excelWriter.setCellValue(CellAddress.colA_row1("E", 9 + k), getAggregatedPayoff(options.toArray(new SingleOption[options.size()]), 1000000.0));
        // set TTM axis labels
        excelWriter.setRangeValues(CellAddress.colA_row1("C", 25), getTtmAxisLabels(formattedResults.size()));
        // save matrix with price surface
        excelWriter.setRangeValues(CellAddress.colA_row1("D", 25), formattedResults);
        // back to main sheet
        excelWriter.selectSheet("FINAL-REPORT");
        excelWriter.save();
        excelWriter.close();
    }

    private List<Object[]> getTtmAxisLabels(int numberOfPointsOnTtmAxis) {
        List<Object[]> ttmAxisLabels = new ArrayList<>(numberOfPointsOnTtmAxis);
        double ttmPercentageStep = 1.0 / (numberOfPointsOnTtmAxis - 1);
        for (int i = 0; i < numberOfPointsOnTtmAxis; i++) {
            ttmAxisLabels.add(new Object[]{i * ttmPercentageStep});
        }
        return ttmAxisLabels;
    }

    private double getAggregatedPayoff(SingleOption[] singleOptions, double settlementPrice) {
        return Arrays.stream(singleOptions)
                .mapToDouble(x -> x.calculatePayoff(settlementPrice) - x.getCurrentPrice())
                .sum();
    }

    public Path getFinalReport() {
        return finalReport;
    }

    private void exportResource(String resourceName) {
        InputStream stream = null;
        OutputStream resStreamOut = null;

        try {
            stream = getClass().getClassLoader().getResourceAsStream(resourceName);
            int readBytes;
            byte[] buffer = new byte[4096];

            resStreamOut = new FileOutputStream(finalReport.toFile());
            while ((readBytes = stream.read(buffer)) > 0) {
                resStreamOut.write(buffer, 0, readBytes);
            }
            stream.close();
            resStreamOut.close();
        } catch (Exception ex) {
        }
    }
}
