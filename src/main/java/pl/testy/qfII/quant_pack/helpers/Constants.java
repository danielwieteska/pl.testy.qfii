package pl.testy.qfII.quant_pack.helpers;

public class Constants {
    public static final String rootForFile = "/opt/qfIIserver/";
    public static final String xlsmScheme = "qf-II.xlsm";
    public static final String outputFileName = "qf-II.xlsm";
    public static final int numberOfSpotPricesOnSurface = 101;
    public static final double percentStepOfSpotPricesOnSurface = 0.01;
    public static final double initialSpotPriceOnSurfaceAsPercentOfCurrentSpot = 0.5;
}
