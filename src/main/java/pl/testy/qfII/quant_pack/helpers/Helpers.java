package pl.testy.qfII.quant_pack.helpers;

import java.util.Arrays;

import static java.lang.Math.sqrt;

public class Helpers {
    Helpers() {
    }

    public static double calculateMean(double[] observations) {
        return Arrays.stream(observations).sum() / observations.length;
    }

    public static double calculateStdDev(double[] observations, double mean) {
        double sumOfErrors = 0.0;
        for (double obs : observations) {
            double error = obs - mean;
            sumOfErrors += (error * error);
        }
        return sqrt(sumOfErrors / observations.length);
    }
}
