package pl.testy.qfII.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import pl.testy.qfII.model.BeanNames;
import pl.testy.qfII.model.InputData;
import pl.testy.qfII.model.OutputData;
import pl.testy.qfII.quant_pack.managers.ExcelManager;

import javax.servlet.http.HttpServletRequest;

@Controller
@SessionAttributes(names = {
        BeanNames.INPUT_DATA,
        BeanNames.OUTPUT_DATA
})
public class InputDataController {

    @GetMapping("/")
    public String init() {
        return "redirect:step-1";
    }

    @GetMapping("/step-1")
    public String step1(
            Model model,
            InputData inputData
    ) {
        model.addAttribute(BeanNames.INPUT_DATA, inputData);
        return "input-data-step-1";
    }

    // PRG pattern
    // https://en.wikipedia.org/wiki/Post/Redirect/Get

    // PRG for step-2
    @PostMapping("/step-2")
    public String step2Post(
            @ModelAttribute(name = BeanNames.INPUT_DATA) InputData inputData
    ) {
        return "redirect:step-2";
    }

    @GetMapping("/step-2")
    public String step2Get() {
        return "input-data-step-2";
    }

    // PRG for step-3
    @PostMapping("/step-3")
    public String step3Post(
            @ModelAttribute(name = BeanNames.INPUT_DATA) InputData inputData
    ) {
        return "redirect:step-3";
    }

    @GetMapping("/step-3")
    public String step3Get() {
        return "input-data-step-3";
    }

    // PRG for result
    @PostMapping("/result")
    public String resultPost(
            Model model,
            @ModelAttribute(name = BeanNames.INPUT_DATA) InputData inputData,
            OutputData outputData,
            HttpServletRequest request
    ) {
        ExcelManager excelManager = new ExcelManager(request.getRemoteAddr());
        excelManager.createReportInExcel(inputData);
        //
        outputData.setFinalReportPath(excelManager.getFinalReport());
        model.addAttribute(BeanNames.OUTPUT_DATA, outputData);
        return "redirect:result";
    }

    @GetMapping("/result")
    public String resultGet() {
        return "output-data-result";
    }
}
