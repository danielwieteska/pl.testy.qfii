package pl.testy.qfII.controller;

import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;
import pl.testy.qfII.model.BeanNames;
import pl.testy.qfII.model.OutputData;

import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

@Controller
@SessionAttributes(names = {
        BeanNames.OUTPUT_DATA
})
public class SendFileController {

    @GetMapping("/download")
    public void getFile(
            @ModelAttribute(name = BeanNames.OUTPUT_DATA) OutputData outputData,
            HttpServletResponse response
    ) {
        try {
            InputStream inputStream = new FileInputStream(outputData.getFinalReportPath().toFile());
            String responseHeader = String.format("attachment; filename=\"%s\"", outputData.getFinalReportPath().toFile().getName());
            response.setHeader("Content-Disposition", responseHeader);
            FileCopyUtils.copy(inputStream, response.getOutputStream());
            response.flushBuffer();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
