package pl.testy.qfII.app;

import org.springframework.lang.Nullable;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
import pl.testy.qfII.config.MainConfig;
import pl.testy.qfII.config.ModelBeans;
import pl.testy.qfII.config.ThymeleafConfig;

public class AppInit extends AbstractAnnotationConfigDispatcherServletInitializer {
    @Nullable
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[0];
    }

    @Nullable
    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{
                MainConfig.class,
                ThymeleafConfig.class,
                ModelBeans.class
        };
    }

    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }
}
