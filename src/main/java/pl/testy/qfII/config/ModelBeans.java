package pl.testy.qfII.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.ModelAttribute;
import pl.testy.qfII.model.BeanNames;
import pl.testy.qfII.model.InputData;
import pl.testy.qfII.model.OutputData;

@Configuration
public class ModelBeans {

    @ModelAttribute(BeanNames.INPUT_DATA)
    public InputData getInputData() {
        return new InputData();
    }

    @ModelAttribute(BeanNames.OUTPUT_DATA)
    public OutputData getOutputData() {
        return new OutputData();
    }
}

